package com.sideway.a2gucar.bluetooth;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Context;

import com.sideway.a2gucar.dialogs.DaguDialog;

import java.util.ArrayList;

public class BluetoothDialog extends DaguDialog {

    public BluetoothDialog(Context c) {
        super(c);

        this.setTitle("Bluetooth");

        CharSequence[] opts = {"Select device", "Disconnect"};
        this.setOptions(opts);
    }

    @Override
    public void onOptionSelected(String a) {
        System.out.println("Choice : " + a);

        BluetoothAdapter adp = BluetoothAdapter.getDefaultAdapter();

        System.out.println("-> " + adp.getBondedDevices().size());
    }
}
