package com.sideway.a2gucar.bluetooth;


import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;

import java.io.DataOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.util.Set;
import java.util.UUID;

public class BluetoothWorker extends Thread {

    private BluetoothAdapter adapter;
    private Set<BluetoothDevice> devices;
    private DataOutputStream stream;
    private BluetoothSocket sock;

    public BluetoothWorker() {
        adapter = BluetoothAdapter.getDefaultAdapter();
        devices = adapter.getBondedDevices();
    }

    public Set<BluetoothDevice> getDevices() {
        return devices;
    }

    public boolean connectToMac(String mac) {
        for (BluetoothDevice device : devices) {
            if (device.getAddress().equals(mac)) {
                System.out.println(device.getAddress());
                connect(adapter.getRemoteDevice(mac));
            }
        }

        return false;
    }

    private boolean connect(BluetoothDevice device) {
        try {
            adapter.cancelDiscovery();
            sock = device.createRfcommSocketToServiceRecord(UUID.fromString("00001101-0000-1000-8000-00805f9b34fb"));
            sock.connect();
            stream = new DataOutputStream(sock.getOutputStream());

        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    public void write(byte opcode) {
        try {
            if (stream != null) {
                stream.writeByte(opcode);
                stream.flush();
                System.out.println("Writing : " + opcode);
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    @Override
    public void run() {

    }


}
