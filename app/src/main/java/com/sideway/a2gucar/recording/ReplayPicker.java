package com.sideway.a2gucar.recording;


import android.app.Activity;
import android.content.DialogInterface;
import android.os.Environment;
import android.support.v7.app.AlertDialog;

import com.sideway.a2gucar.ManagerInterface;

import java.io.File;
import java.util.ArrayList;

/**
 * Class Letting the user choose a replay file
 */
public class ReplayPicker {

    private AlertDialog.Builder menu;
    private Activity act;

    private ArrayList<String> paths;


    public ReplayPicker(Activity act) {
        menu = new AlertDialog.Builder(act);
        menu.setTitle("Pick replay");
        this.act = act;
    }

    public void getChoice(final String mode) {
        File f = new File(Environment.getExternalStorageDirectory() + "/A2GU/replays/");
        paths = new ArrayList<String>();

        for (File v : f.listFiles()) {
            paths.add(v.getPath());
        }

        menu.setItems(paths.toArray(new String[paths.size()]), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String path = paths.get(which);
                if (mode == "delete") {
                    File f = new File(path);
                    f.delete();
                }

                if (mode == "load") {
                    ManagerInterface l = (ManagerInterface) act;
                    l.play(path);
                }
            }
        });

        menu.show();
    }
}
