package com.sideway.a2gucar.recording;

import android.graphics.Path;
import android.os.Environment;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;

public class InputRecorder {

    private long start_time;
    private OpcodeEvent current;
    private boolean is_running;

    private ArrayList<OpcodeEvent> events;

    public InputRecorder() {
        current = new OpcodeEvent(0, (byte) 0x00);
        events = new ArrayList<OpcodeEvent>();
        is_running = false;
    }

    public void start() {
        if (!events.isEmpty()) {
            events.clear();
        }

        is_running = true;
        start_time = System.currentTimeMillis();
        current = new OpcodeEvent(start_time, (byte) 0x00);
    }

    public void stop() {
        is_running = false;
    }

    public void update(byte opcode) {
        if (is_running) {
            long dt = System.currentTimeMillis() - start_time;
            long prev = System.currentTimeMillis() - current.getTime();

            OpcodeEvent processed_evt = new OpcodeEvent(dt, opcode);

            if (opcode != current.getValue()) {
                events.add(processed_evt);
                current = processed_evt;
            }

            if (opcode == current.getValue() && prev > 50) {
                events.add(processed_evt);
                current = processed_evt;
            }
        }
    }

    public ArrayList<OpcodeEvent> getEvents() {
        return events;
    }

    public void dump() throws IOException {
        Calendar time = Calendar.getInstance();
        String filename = time.get(time.DAY_OF_MONTH) + "-" +
                time.get(time.MONTH) + "-" +
                time.get(time.YEAR) + "_" +
                time.get(time.HOUR) + "h" +
                time.get(time.MINUTE) + "m" +
                time.get(time.SECOND) + "s.dpf";
        File f = new File(Environment.getExternalStorageDirectory() + "/A2GU/replays/" + filename);

        DataOutputStream stream = new DataOutputStream(new FileOutputStream(f));

        for (OpcodeEvent op : events) {
            stream.writeLong(op.getTime());
            stream.writeByte(op.getValue());
        }

        stream.flush();
        stream.close();
    }

    public boolean isRunning() {
        return is_running;
    }
}
