package com.sideway.a2gucar.recording;


/**
 * Callback interface for classes using an InputRecorder
 */
public interface ReplayInterface {

    public void record();

    public void stop();

    public void save();

    public void delete();

    public void load();
}
