package com.sideway.a2gucar.recording;

public class OpcodeEvent {


    private long time;
    private byte value;

    public OpcodeEvent(long time, byte value) {
        this.time = time;
        this.value = value;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public byte getValue() {
        return value;
    }

    public void setValue(byte value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Opcode [" + time + " " + value + "]";
    }
}
