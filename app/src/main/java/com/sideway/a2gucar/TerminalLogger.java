package com.sideway.a2gucar;

import android.text.method.ScrollingMovementMethod;
import android.widget.TextView;

public class TerminalLogger {

    private TextView terminal;
    private String logtext;

    public TerminalLogger(TextView tv) {
        logtext = "";
        terminal = tv;
        terminal.setMovementMethod(new ScrollingMovementMethod());
    }

    public void log(String name, String content) {
        String data = "[" + name + "] " + content + "\n";

        terminal.append(data);
        logtext += data;
    }

    public String getLogtext() {
        return logtext;
    }
}
