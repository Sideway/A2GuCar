package com.sideway.a2gucar.replaying;

public interface OpcodeReceiver {

    public void send(byte opcode);
}
