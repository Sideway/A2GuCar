package com.sideway.a2gucar.replaying;


import android.app.ProgressDialog;
import android.os.AsyncTask;

import com.sideway.a2gucar.Utils;
import com.sideway.a2gucar.recording.OpcodeEvent;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Stack;

/**
 * Object replaying data to object implementing OpcodeReceiver interface
 */

public class AsyncReplayer extends AsyncTask<String, Integer, Boolean> {

    private ProgressDialog progress_dialog;
    private OpcodeReceiver receiver;
    private int total_opcodes = 0;

    public AsyncReplayer(ProgressDialog p, OpcodeReceiver receiver) {
        progress_dialog = p;
        this.receiver = receiver;
    }

    @Override
    protected Boolean doInBackground(String... params) {
        Stack<OpcodeEvent> ops = parseFile(params[0]);

        if (!ops.isEmpty()) {
            total_opcodes = ops.size();
            long start = System.currentTimeMillis();
            long time = System.currentTimeMillis() - start;

            progress_dialog.setMax(total_opcodes);

            while (ops.size() > 0) {
                OpcodeEvent evt = ops.pop();

                while (time <= evt.getTime()) {
                    time = System.currentTimeMillis() - start;
                }

                receiver.send(evt.getValue());

                this.publishProgress(total_opcodes - ops.size());
            }
        }

        return false;
    }

    @Override
    protected void onProgressUpdate(Integer... value) {
        progress_dialog.setProgress(value[0]);
    }

    private Stack<OpcodeEvent> parseFile(String path) {
        Stack<OpcodeEvent> temp_stack = new Stack<OpcodeEvent>();

        try {
            File f = new File(path);
            DataInputStream dis = new DataInputStream(new FileInputStream(f));

            for (int i = 0; i < (f.length() / 9); i++) {
                long time = dis.readLong();
                byte op = dis.readByte();

                temp_stack.add(new OpcodeEvent(time, op));
            }

            dis.close();
        } catch (IOException e) {
            e.printStackTrace();
            return temp_stack;
        }

        Stack<OpcodeEvent> result = new Stack<OpcodeEvent>();

        while (!temp_stack.isEmpty()) {
            result.push(temp_stack.pop());
        }

        return result;
    }

    @Override
    protected void onPostExecute(Boolean result) {
        progress_dialog.cancel();
    }
}
