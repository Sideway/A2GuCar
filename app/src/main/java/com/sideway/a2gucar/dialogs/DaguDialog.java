package com.sideway.a2gucar.dialogs;

import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;


/**
 * Wrapper around Android dialogs to make code "cleaner"
 */

public abstract class DaguDialog extends AlertDialog.Builder {

    protected CharSequence[] options;

    public DaguDialog(Context context) {
        super(context);
    }

    public void setOptions(CharSequence[] items) {
        options = items;
        this.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                onOptionSelected(options[which].toString());
            }
        });
    }

    /**
     * Wrapper called when an item is clicked
     */
    public void onOptionSelected(String item) {

    }
}
