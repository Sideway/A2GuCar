package com.sideway.a2gucar.dialogs;

import android.content.Context;

import com.sideway.a2gucar.recording.ReplayInterface;

public class RecorderDialog extends DaguDialog {

    private ReplayInterface manager;

    public RecorderDialog(Context c) {
        super(c);

        manager = (ReplayInterface) c;
        this.setTitle("Recorder");
        CharSequence[] v = {"Record", "Stop", "Play", "Save", "Delete"};
        this.setOptions(v);
    }

    @Override
    public void onOptionSelected(String a) {
        switch (a) {
            case "Record":
                manager.record();
                break;
            case "Stop":
                manager.stop();
                break;
            case "Save":
                manager.save();
                break;
            case "Delete":
                manager.delete();
                break;

            case "Play":
                manager.load();
                break;
        }
    }


}
