package com.sideway.a2gucar;

import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.DialogInterface;
import android.content.pm.ActivityInfo;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.util.UUID;

import android.widget.Toast;

import com.sideway.a2gucar.bluetooth.BluetoothDialog;
import com.sideway.a2gucar.bluetooth.BluetoothWorker;
import com.sideway.a2gucar.dialogs.RecorderDialog;
import com.sideway.a2gucar.recording.InputRecorder;
import com.sideway.a2gucar.recording.ReplayInterface;
import com.sideway.a2gucar.recording.ReplayPicker;
import com.sideway.a2gucar.replaying.AsyncReplayer;
import com.sideway.a2gucar.replaying.OpcodeReceiver;

import co.lujun.lmbluetoothsdk.BluetoothController;
import io.github.controlwear.virtual.joystick.android.JoystickView;


public class MainActivity extends AppCompatActivity implements ReplayInterface, OpcodeReceiver, ManagerInterface {


    private JoystickView joystick;
    private JoystickHandler handler;
    private TerminalLogger term;

    private CharSequence[] choices = new CharSequence[]{"Connect", "Replays", "About"};

    private TextView speed_view;
    private TextView angle_view;
    private TextView result_view;

    private InputRecorder recorder;
    private ReplayPicker picker;

    // Dialogs
    private RecorderDialog recorder_dialog;
    private AlertDialog.Builder main_settings;
    private BluetoothDialog bg;

    // Debug shit
    private BluetoothWorker worker;

    private BluetoothController c;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //testBt();
        worker = new BluetoothWorker();
        worker.connectToMac("20:13:04:23:16:40");
        worker.start();

        // Setting up folders
        if (Utils.mkdir("/A2GU/replays")) {
            System.out.println("Folder created !");
        } else {
            Toast.makeText(this, "Error creating folder", Toast.LENGTH_SHORT);
        }

        // Quick hack to hide status bar
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);

        setContentView(R.layout.activity_main);
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        // Init the TextView display
        speed_view = (TextView) findViewById(R.id.speed);
        angle_view = (TextView) findViewById(R.id.angle);
        result_view = (TextView) findViewById(R.id.result);

        // Init the recorder interface
        recorder = new InputRecorder();
        picker = new ReplayPicker(this);


        // Debug shit
        recorder_dialog = new RecorderDialog(this);
        bg = new BluetoothDialog(this);

        main_settings = new AlertDialog.Builder(this);
        main_settings.setTitle("Settings");
        main_settings.setItems(choices, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String choice = choices[which].toString();

                switch (choice) {
                    case "Replays":
                        recorder_dialog.show();
                        break;
                    case "Connect":
                        //   for(BluetoothDevice i : worker.getDevices()){
                        //      System.out.println(i.getName()+" "+i.getAddress());
                        // }
                        break;
                    case "About":
                        term.log("About", "App by sideway < dev.sideway@gmail.com >");
                        break;
                }
            }
        });

        ImageView im = (ImageView) findViewById(R.id.settings);

        // More debug shit
        im.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                main_settings.show();
            }
        });

        TextView tv = (TextView) findViewById(R.id.terminal);
        term = new TerminalLogger(tv);
        term.log("Main", "Starting log handler");

        handler = new JoystickHandler();
        joystick = (JoystickView) findViewById(R.id.joystick);

        joystick.setScaleX(0.75f);
        joystick.setScaleY(0.75f);
        joystick.setOnMoveListener(new JoystickView.OnMoveListener() {
            @Override
            public void onMove(int angle, int strength) {
                byte op = handler.getResult();

                handler.update(angle, strength);
                recorder.update(handler.getResult());

                speed_view.setText("Speed : " + handler.getSpeed());
                angle_view.setText("Position : " + handler.getAngle());
                result_view.setText("Opcode : " + Utils.byteToHex(handler.getResult()));

                // System.out.println(handler.getResult());

                send(handler.getResult());
            }
        });

        term.log("Main", "Finished joystick setup !");
    }

    @Override
    public void record() {
        if (!recorder.isRunning()) {
            recorder.start();
        } else {
            term.log("Recorder", "Recorder already running !");
        }
    }

    @Override
    public void stop() {
        if (recorder.isRunning()) {
            recorder.stop();
            term.log("Recorder", "Recorded " + recorder.getEvents().size() + " opcodes");
        } else {
            term.log("Recorder", "No recording to stop");
        }
    }

    @Override
    public void save() {
        try {
            recorder.dump();
            term.log("Recorder", "Size on device " + recorder.getEvents().size() * 9 + "b");
        } catch (IOException e) {
            term.log("Recorder", "Saving failed :(");
        }
    }

    @Override
    public void delete() {
        picker.getChoice("delete");
    }

    @Override
    public void load() {
        picker.getChoice("load");
    }

    @Override
    public void send(byte opcode) {
        // Stub for now but it will later contain the bluetooth part
        worker.write(opcode);
    }

    @Override
    public void play(String path) {
        // Creating the progress bar
        ProgressDialog p = new ProgressDialog(this);
        p.setCancelable(false);
        p.setTitle("Sending opcodes ...");
        p.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        p.show();

        // Launching our replayer
        AsyncReplayer player = new AsyncReplayer(p, this);
        player.execute(path);
    }

    @Override
    public void log(String title, String text) {

    }

    private void testBt() {
        //dev = BlueteethManager.with(this).getPeripheral("20:13:04:23:16:40");
    }

}
