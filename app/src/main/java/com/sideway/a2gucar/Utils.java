package com.sideway.a2gucar;

import android.os.Environment;

import java.io.File;

/**
 * Created by root on 3/23/17.
 */

public class Utils {

    public static boolean mkdir(String path) {
        File f = new File(Environment.getExternalStorageDirectory() + path);
        System.out.println(f.getAbsolutePath());
        return f.mkdirs();
    }

    public static String byteToHex(byte value) {
        char[] hexchars = {'0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'};
        String result = "0x";

        result += hexchars[(value >> 4) & 0x0f];
        result += hexchars[value & 0x0f];

        return result;
    }
}
