package com.sideway.a2gucar;

import io.github.controlwear.virtual.joystick.android.JoystickView;

public class JoystickHandler {

    private byte b_angle;
    private byte b_speed;
    private byte b_result;

    private byte[] pos_range = {0x4, 0x6, 0x1, 0x5, 0x3, 0x7, 0x2, 0x8};
    private byte[] speed_range = {0x0, 0x1, 0x2, 0x3, 0x4, 0x5, 0x6, 0x7, 0x8, 0x9, 0xa, 0xb, 0xc, 0xd, 0xe, 0xf};

    public JoystickHandler() {
        b_angle = 0;
        b_speed = 0;
        b_result = 0;
    }

    public void update(double angle, double strength) {
        // Fixing the out of zone glitch
        int angle_index = (int) Math.round(angle * (pos_range.length - 1) / 360);
        int speed_index = (int) Math.round(strength * (speed_range.length - 1) / 100);

        if (angle_index > (pos_range.length - 1)) {
            angle_index = 0;
        }

        if (speed_index > (speed_range.length - 1)) {
            speed_index = 0;
        }

        b_angle = pos_range[angle_index];
        b_speed = speed_range[speed_index];

        if (b_speed == 0) {
            b_angle = 0;
        }

        b_result = (byte) ((b_angle << 4) | b_speed);
    }

    public byte getResult() {
        return b_result;
    }

    public byte getSpeed() {
        return b_speed;
    }

    public byte getAngle() {
        return b_angle;
    }
}
