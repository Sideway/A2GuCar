package com.sideway.a2gucar;

public interface ManagerInterface {

    public void play(String path);

    public void log(String title, String text);
}
